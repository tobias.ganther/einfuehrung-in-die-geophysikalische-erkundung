## Specify parent image. Please select a fixed tag here.
#
##ARG BASE_IMAGE=registry.git.rwth-aachen.de/jupyter/profiles/rwth-courses:2020-ss.1
#ARG BASE_IMAGE=registry.git-ce.rwth-aachen.de/jupyter/singleuser/python:latest
#FROM ${BASE_IMAGE}
#
#ADD environment.yml /tmp/environment.yml
#
#RUN conda env update -f /tmp/environment.yml && \
#    conda clean -a -f -y




# Install packages via requirements.txt
ADD requirements.txt .
RUN pip install -r requirements.txt

## .. Or update conda base environment to match specifications in environment.yml
#ADD environment.yml /tmp/environment.yml
#
## All packages specified in environment.yml are installed in the base environment
#RUN conda env update -f /tmp/environment.yml && \
#    conda clean -a -f -y
#
#ENV JUPYTER_ENABLE_LAB=yes

#ADD environment.yml /tmp/environment.yml
#RUN conda env create -f /tmp/environment.yml
##RUN conda activate geosci-labs
#RUN conda init geosci-labs
#RUN ipython kernel install --user --name=geosci-labs
##RUN conda deactivate
#ENV JUPYTER_ENABLE_LAB=yes
#    #conda install -c anaconda ipykernel
#    #python -m ipykernel install --user --name=geosci-labs


