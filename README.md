# Jupyter Notebooks für den Kurs 'Einführung in die Geophysikalische Erkundung'

Diese Jupyter Notebooks sind begleitendes Lehrmaterial für den RWTH Kurs <u>"Physik der Erde"</u>. Es werden verschiedene in den Vorlesungen unterrichtete Themen behandelt. Sämtliche Notebooks laufen direkt auf dem <u>JupyterHub</u> ohne die Notwendigkeit, Python auf dem eigenen Rechner installiert zu haben (einfach auf die jeweiligen Notebooks links in der Leiste klicken). Alternativ können die Notebooks auch <u>lokal</u> verwendet werden. Allerdings benötigt man hierfür gewisse <u>geophysikalische Programme</u>. Die Installation kann durch eingefügte Befehle, welche <u>unten im Reader</u> gegeben sind, durchgeführt werden (Befehle entkommentieren und Zelle laufen lassen). Dies ist nur einmal nötig. Im JupyterHub laufen die Notebooks direkt und benötigen diese Befehle <u>NICHT</u>. 

# Folgende Notebooks stehen zur Verfügung:

### fourier-Transformation.ipynb
Dieses Notebook beschäftigt sich mit der Fourier-Transformationen und erläutert deren Prinzipien.

### Seis_NMO.ipynb
Dieses Notebook beschäftigt sich mit dem normal moveout (NMO), welches die Änderung der Laufzeit einer Reflexion mit zunehmendem Schuß-Geophon-Abstand relativ zur Laufzeit beim Abstand Null darstellt.

### Seis_Reflection.ipynb
In diesem Notebook kann man sich mit Reflexionsseismik beschäftigen.

### Seis_Refraction.ipynb
In diesem Notebook kann man sich mit Refraktionsseismik beschäftigen.

### Vertical_Resolution.ipynb
Tiefenauflösung und Forwärtsmodellierung.

### Seis_Applet.ipynb
Allegemeines Notebook, welches Strahlenwege und Geschwindigkeitsdiagramme zeigt.

